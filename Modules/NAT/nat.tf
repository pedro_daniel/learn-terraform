resource "aws_nat_gateway" "nat-gw" {
  count = var.counter

  depends_on = [var.depends]
  allocation_id = element(var.eip_id[*], count.index)
  subnet_id = element(var.subnet_id[*], count.index)

  tags = {
    Name = "nat-${var.natTagName}"
  }
}