resource "aws_network_acl" "acl" {
  vpc_id = var.vpc_id
  subnet_ids = var.subnet_ids

  egress {
    cidr_block = "0.0.0.0/0"
    protocol = "-1"
    rule_no = 50
    from_port = 0
    to_port = 0
    action = "allow"

  }

  ingress {
    cidr_block = "0.0.0.0/0"
    protocol = "-1"
    rule_no = 50
    from_port = 0
    to_port = 0
    action = "allow"
  }

  tags = {
      Name = "test-Acls"
  }
}
