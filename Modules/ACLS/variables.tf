variable "subnet_ids" {
  description = "Ids of the subnets to apply the acl"
}

variable "vpc_id" {
  description = "Vpc id of the acl"
}
