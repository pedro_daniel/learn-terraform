resource "aws_eip" "eip" {
  count = var.counter
  vpc = true
  tags = {
      Name = "eip-${var.eipTagName}-${count.index}"
  }
}
