resource "aws_security_group_rule" "sg_rule" {
  type = "ingress"
  cidr_blocks = var.cidr
  from_port = var.from_port
  to_port = var.to_port
  protocol = var.protocol
  security_group_id = var.sg_id
}
