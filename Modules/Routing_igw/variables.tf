variable "counter_pub" {
  description = "Number if routing tables"
}

variable "vpc_id" {
 description = "Id of the vpc" 
}

variable "subnet_pub_id" {
  description = "Public Subnets"
}

variable "tagName" {
  description = "Tag name"
}

variable "igw" {
  description = "Internet gateway for the public subnets"
}
