resource "aws_route_table" "rt_pub" {
  count = var.counter_pub
  vpc_id = var.vpc_id
  
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = var.igw
  }

  tags = {
    Name = "rt-pub-${var.tagName}-0${count.index +1}"
  }
}

resource "aws_route_table_association" "ass_pub" {
  count = var.counter_pub
  subnet_id = element( var.subnet_pub_id[*] ,count.index)
  route_table_id = element( aws_route_table.rt_pub[*].id ,count.index)
}
