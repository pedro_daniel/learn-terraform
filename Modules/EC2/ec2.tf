resource "aws_instance" "ec2" {
  ami           = var.ami
  instance_type = "t2.micro"
  key_name  = var.key
  subnet_id = var.subnet_id
  associate_public_ip_address = var.public_ip
  security_groups  = var.sg

  tags = {
    Name = "${var.tagName}-ec2"
  }
}