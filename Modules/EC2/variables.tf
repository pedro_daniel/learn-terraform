variable "ami" {
  description = "Ami for the ec2"
}

variable "key" {
  description = "Ec2 key"
}

variable "subnet_id" {
  description = "Private to set the ec2"
}

variable "tagName" {
  description = "Tag name for the prv ec2"
}

variable "sg" {
  description = "Security group of the ec2"
}

variable "public_ip" {
  description = "If it has a public ip"
}

