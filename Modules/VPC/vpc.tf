resource "aws_vpc" "vpc"{
    cidr_block = var.ip

    tags = {
        Name = var.VpcTagName
    }
}
