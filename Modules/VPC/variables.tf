variable "ip" {
  description = "Ip for the VPC"
}

variable "VpcTagName" {
  description = "Name for the vpc Tag"
}
