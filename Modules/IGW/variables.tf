variable "vpc_id" {
  description = "VPC to connect the igw to"
}

variable "tagName" {
  description = "Name tag igw"
}
