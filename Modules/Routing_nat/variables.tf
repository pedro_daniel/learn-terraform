variable "counter_prv" {
  description = "Number if routing tables"
}

variable "vpc_id" {
 description = "Id of the vpc" 
}

variable "subnet_prv_id" {
  description = "Private subnets"
}

variable "nat" {
  description = "Nat gateway for the privates subnets"
}
variable "tagName" {
  description = "Tag name"
}