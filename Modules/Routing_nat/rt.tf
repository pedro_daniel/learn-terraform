resource "aws_route_table" "rt_prv" {
  count = var.counter_prv
  vpc_id = var.vpc_id
  
  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = element(var.nat[*],count.index)
  }
  tags = {
    Name = "rt-prv-${var.tagName}-0${count.index +1}"
  }
}

resource "aws_route_table_association" "ass_prv" {
  count = var.counter_prv
  subnet_id = element( var.subnet_prv_id[*] ,count.index)
  route_table_id = element( aws_route_table.rt_prv[*].id ,count.index)
}