output "subnets_prv_id" {
  value = aws_subnet.subnets_prv[*].id
}

output "subnets_pub_id" {
  value = aws_subnet.subnets_pub[*].id
}