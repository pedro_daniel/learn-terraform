resource "aws_subnet" "subnets_pub"{
    count = var.counter
    vpc_id = var.vpc_id
    cidr_block = element(var.subnet_ip_pub, count.index )
    availability_zone_id = element(var.availability_zone, count.index )
    map_public_ip_on_launch = true

    tags = {
        Name = "${var.SubnetTagName}-pub-0${count.index}"
    }
}

resource "aws_subnet" "subnets_prv"{
    count = var.counter
    vpc_id = var.vpc_id
    cidr_block = element(var.subnet_ip_prv, count.index )
    availability_zone_id = element(var.availability_zone, count.index )
    map_public_ip_on_launch = false

    tags = {
        Name = "${var.SubnetTagName}-prv-0${count.index}"
    }
}
