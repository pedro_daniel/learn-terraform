variable "SubnetTagName"{
  description = "Name for the subnet Tag"
}

variable "vpc_id"{
  description = "Id of the vpc"
}

variable "subnet_ip_prv" {
  description = "Cidr for the subnet"
}

variable "subnet_ip_pub" {
  description = "Cidr for the subnet"

}

variable "counter" {
  description = "Number of subnets"
}

variable "availability_zone" {
  description = "availability zones"
}


