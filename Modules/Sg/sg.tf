resource "aws_security_group" "sg" {
  name = var.name
  description = var.description
  vpc_id = var.vpc
  ingress {
      from_port = var.ingress_port
      to_port  = var.ingress_port
      protocol = "tcp"
      cidr_blocks = var.ingress_cidr
  }

    egress {
      from_port = 0
      to_port  = 0
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
      Name = "${var.tagName}-sg"
  }
}

