variable "name" {
  description = "Sg name"
}

variable "description" {
  description = "sg description"
}

variable "vpc" {
  description = "Vpc id of the security group"
}

variable "ingress_port" {
  description = "ingress to allow"
}

variable "ingress_cidr" {
  description = "cidr array to allow"
}

variable "tagName" {
  description = "Name for the tag"
}
