module "vpc" {
  source = "../Modules/VPC"

  ip = var.ip
  VpcTagName = "Learn VPC"
}

module "vpc_igw" {
  source = "../Modules/IGW"

  vpc_id = module.vpc.vpc_id

  tagName = "Learn VPC"
}

module "subnets_prv_pub" {
    source = "../Modules/Subnets"
    counter = length(var.subnet_ip_pub)

    availability_zone = var.availability_zone
    vpc_id = module.vpc.vpc_id
    SubnetTagName = "test-subnet"
    subnet_ip_pub = var.subnet_ip_pub
    subnet_ip_prv = var.subnet_ip_prv
}

module "eip" {
  source = "../Modules/EIP"
  counter = 3
  eipTagName = "test"
}

module "nats" {
  source = "../Modules/NAT"
  
  depends = module.vpc_igw
  counter = length(var.subnet_ip_prv)
  eip_id = module.eip.eip_id
  subnet_id = module.subnets_prv_pub.subnets_pub_id
  natTagName = "test"
}

module "routing_tables_prv" {
  source = "../Modules/Routing_nat"
  
  counter_prv = length(var.subnet_ip_prv)
  vpc_id = module.vpc.vpc_id
  subnet_prv_id = module.subnets_prv_pub.subnets_prv_id
  nat = module.nats.nat_id
  tagName = "learn"
}

module "routing_tables_pub" {
  source = "../Modules/Routing_igw"
  
  counter_pub = length(var.subnet_ip_pub) 
  vpc_id = module.vpc.vpc_id
  subnet_pub_id = module.subnets_prv_pub.subnets_pub_id
  igw = module.vpc_igw.igw_id
  tagName = "learn"
}


module "acls" {
  source = "../Modules/ACLS"
  vpc_id = module.vpc.vpc_id
  subnet_ids = concat(module.subnets_prv_pub.subnets_prv_id , module.subnets_prv_pub.subnets_pub_id)
}

