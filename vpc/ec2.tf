module "ec2_pub" {
  source = "../Modules/EC2"
  ami = var.ami
  key = var.key
  subnet_id = module.subnets_prv_pub.subnets_pub_id[0]
  tagName = "learn-pub"
  sg = [module.sg.sg_id]
  public_ip = true
}

module "ec2_prv1" {
  source = "../Modules/EC2"
  ami = var.ami
  key = var.key
  subnet_id = module.subnets_prv_pub.subnets_prv_id[0]
  tagName = "learn-prv-nat"
  sg = [module.sg.sg_id]
  public_ip = false
}


module "sg" {
  source = "../Modules/Sg"
  
  name  = var.sg_name
  description = var.sg_description
  vpc = module.vpc.vpc_id
  ingress_port = var.ingress_port
  ingress_cidr = var.ingress_cidr
  tagName = "learn-prv"
}

module "sg_rule" {
  source = "../Modules/Sg_rule"
  
  cidr = ["10.255.0.0/16"]
  from_port = 0
  to_port = 0
  sg_id = module.sg.sg_id
  protocol = "-1"
}
