#####
#EC2#
#####

variable "ami" {
  default = "ami-0b418580298265d5c"
}

variable "key" {
  default = "learn"
}

variable "sg_name" {
  default = "learn-sg"
}

variable "sg_description" {
  default = "learn security group"
}

variable "ingress_port" {
  default = 22
}

variable "ingress_cidr" {
  default = ["0.0.0.0/0"]
}

#####
#VPC#
#####
variable "ip" {
  default = "10.255.0.0/16"
}

variable "subnet_ip_pub" {
  default = ["10.255.0.0/24","10.255.1.0/24","10.255.2.0/24"]
}

variable "subnet_ip_prv" {
  default = ["10.255.255.0/24","10.255.254.0/24","10.255.253.0/24"]
}


variable "availability_zone" {
  default = ["euc1-az1","euc1-az2","euc1-az3"]
}
