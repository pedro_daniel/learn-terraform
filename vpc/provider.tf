provider "aws" {
  region  = "eu-central-1"
  profile = "terraform-learn"
}

terraform {
  backend "s3" {
    bucket = "terraform-learn"
    key = "state/terraform.tfstate"
    region = "eu-central-1"
    profile = "terraform-learn"
  }
}